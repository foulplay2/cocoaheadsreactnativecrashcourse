import {
  TOGGLE_TODO
} from './types';

export const toggleTodo = (id) => {
  return {
    type: TOGGLE_TODO,
    payload: id
  };
};
