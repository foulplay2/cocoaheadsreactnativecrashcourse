import {
  UPDATE_TEXT,
  ADD_TODO
} from './types';

export const updateText = (text) => {
  return {
    type: UPDATE_TEXT,
    payload: text
  };
};

export const addTodo = (text) => {
  return {
    type: ADD_TODO,
    payload: text
  };
};
