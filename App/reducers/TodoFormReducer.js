import {
  UPDATE_TEXT,
  ADD_TODO
} from '../actions/types';

const INITIAL_STATE = {
  title: ''
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case UPDATE_TEXT:
      return {
        ...state,
        title: action.payload
      };
    case ADD_TODO:
      return INITIAL_STATE;
    default:
      return state;
  }
};
