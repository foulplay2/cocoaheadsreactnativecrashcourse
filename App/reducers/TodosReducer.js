import {
  ADD_TODO,
  TOGGLE_TODO
} from '../actions/types';
import TodoReducer from './TodoReducer';

const INITIAL_STATE = [];

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ADD_TODO:
      return [
        ...state,
        TodoReducer(undefined, action)
      ];
    case TOGGLE_TODO:
      return state.map((todo) => TodoReducer(todo, action));
    default:
      return state;
  }
};
