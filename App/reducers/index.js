import { combineReducers } from 'redux';
import TodoFormReducer from './TodoFormReducer';
import TodosReducer from './TodosReducer';

export default combineReducers({
	todoForm: TodoFormReducer,
  todos: TodosReducer
});
