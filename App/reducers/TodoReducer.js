import {
  ADD_TODO,
  TOGGLE_TODO
} from '../actions/types';

const INITIAL_STATE = {
  title: '',
  completed: false,
  id: null
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ADD_TODO:
      return {
        title: action.payload,
        completed: false,
        id: Date.now()
      };
    case TOGGLE_TODO:
      if (state.id === action.payload) {
        return {
          ...state,
          completed: !state.completed
        };
      }
      return state;
    default:
      return state;
  }
};
