import React, { Component } from 'react';
import { Text } from 'react-native';
import { Card, CardItem, InputGroup, Input, Button } from 'native-base';
import { connect } from 'react-redux';
import { updateText, addTodo } from '../actions';

class TodoForm extends Component {
  render() {
    console.log(this.props);

    return (
      <Card style={{ margin: 15 }}>
        <CardItem>
          <InputGroup>
            <Input
              onChangeText={(text) => this.props.updateText(text)}
              value={this.props.title}
              placeholder="Todo Text..."
            />
          </InputGroup>
        </CardItem>
        <CardItem>
          <Button block onPress={() => this.props.addTodo(this.props.title)}>
            Add Task
          </Button>
        </CardItem>
      </Card>
    );
  }
}

const mapStateToProps = (state) => {
  console.log(state);
  return {
    ...state.todoForm
  };
};

export default connect(mapStateToProps, { updateText, addTodo })(TodoForm);
