import React, { Component } from 'react';
import { View, ListItem, Text } from 'native-base';
import { connect } from 'react-redux';
import { toggleTodo } from '../actions';

class TodoList extends Component {
  render() {
    return (
      <View>
        {this.props.todos.map((todo) => {
          return (
            <ListItem key={todo.id} button onPress={() => this.props.toggleTodo(todo.id)}>
              <Text>
                {todo.title} - {todo.completed ? 'Completed' : 'Incomplete'}
              </Text>
            </ListItem>
          );
        })}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    todos: state.todos
  };
};

export default connect(mapStateToProps, { toggleTodo })(TodoList);
