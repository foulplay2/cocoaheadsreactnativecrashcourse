import React, { Component } from 'react';
import { Container, Header, Title, Content } from 'native-base';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducers from './reducers';
import TodoForm from './components/TodoForm';
import TodoList from './components/TodoList';

class App extends Component {
  render() {
    const store = createStore(reducers, {});
    return (
      <Provider store={store}>
        <Container>
          <Header>
            <Title>Todo App</Title>
          </Header>
          <Content>
            <TodoForm />
            <TodoList />
          </Content>
        </Container>
      </Provider>
    )
  }
}

export default App;
